import numpy as np
import pandas as pd

from tensorflow.python.keras.models import load_model
from tensorflow.python.keras.preprocessing import text, sequence

from train import parameters

import warnings
warnings.filterwarnings('ignore')

if __name__ == '__main__':
    import argparse
    
    
    parser = argparse.ArgumentParser(description="Classify a set of website comments as offensive", 
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-i", "--input", help="input testing comments, without toxic outputs", required = True)
    parser.add_argument("-t", "--train", help="input train comments, with toxic outputs", required = True)
    parser.add_argument("-a", "--augment", help="augmented training input lookup", required = False)    

    parser.add_argument("-w", "--weights", nargs = '+', help="Neural network weights file(s)", required = True)
    parser.add_argument("-s", "--submission", help="submission template", required = True)
    parser.add_argument("-o", "--output", help="input training comments, with toxic outputs", required = True)


    args = parser.parse_args()

    test = pd.read_csv(args.input)
    train = pd.read_csv(args.train)
    submission = pd.read_csv(args.submission)
    
    X = test["comment_text"].fillna("fillna").values
    Xtrain = train["comment_text"].fillna("fillna").values
    
    tokenizer = text.Tokenizer(num_words=parameters.max_features)
    
    if args.augment is not None:
        augment = pd.read_csv(args.augment)
        Xaug = augment["comment_text"].fillna("fillna").values
        tokenizer.fit_on_texts(list(Xtrain) + list(X) + list(Xaug))
    else:
        tokenizer.fit_on_texts(list(Xtrain) + list(X))
    
    Xs = tokenizer.texts_to_sequences(X)
    Xtest = sequence.pad_sequences(Xs, maxlen=parameters.max_length)

    num_models = len(args.weights)
    for model_index in range(num_models):
        model_name = args.weights[model_index]
        model = load_model(model_name)
        print("Predicting model %s" % (model_name))
        if model_index == 0:
            Ypred = model.predict(Xtest, batch_size=32)
        else:
            Ypred += model.predict(Xtest, batch_size=32)
        
    Ypred = Ypred/float(num_models)
        
    submission[["toxic", "severe_toxic", "obscene", "threat", "insult", "identity_hate"]] = Ypred
    submission.to_csv(args.output, index=False)