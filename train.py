import numpy as np
import pandas as pd
import os

from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score
from sklearn.utils import shuffle

from tensorflow.python.keras.models import Model


from tensorflow.python.keras.layers import Input, Dense, Embedding, SpatialDropout1D, concatenate, Dropout, Conv1D
from tensorflow.python.keras.layers import GRU, LSTM, Bidirectional, GlobalAveragePooling1D, GlobalMaxPooling1D
from tensorflow.python.keras.layers import PReLU, BatchNormalization, add, MaxPooling1D
from tensorflow.python.keras.preprocessing import text, sequence
from tensorflow.python.keras.callbacks import Callback, CSVLogger, ModelCheckpoint
from tensorflow.python.keras.optimizers import Adam

import warnings
warnings.filterwarnings('ignore')

def get_model(max_features, maxlen, embedding_matrix):
    inp = Input(shape=(maxlen, ))
    x = Embedding(max_features, embedding_matrix.shape[1], weights=[embedding_matrix], trainable = True)(inp)
    x = SpatialDropout1D(0.2)(x)
    #x = Bidirectional(CuDNNGRU(128, return_sequences=True))(x)
    x = Bidirectional(GRU(128, return_sequences=True))(x)
    avg_pool = GlobalAveragePooling1D()(x)
    max_pool = GlobalMaxPooling1D()(x)
    conc = concatenate([avg_pool, max_pool])
    outp = Dense(6, activation="sigmoid")(conc)
    
    model = Model(inputs=inp, outputs=outp)

    return model

def get_embeddings_filepath():
    embeddings_dirpath = os.path.abspath(os.path.dirname(__file__))
    embeddings_filepath = os.path.join(embeddings_dirpath, 'embeddings.vec')
    return embeddings_filepath

def get_embeddings(filepath, max_features, tokenizer):
    embeddings_index = dict()
    if filepath is None:
        embeddings_filepath = get_embeddings_filepath()
    else:
        embeddings_filepath = filepath
    print("Embeddings file: %s" % (embeddings_filepath))
    embedding_dim = 1
    
    with open(embeddings_filepath, 'r', encoding='utf-8') as embeddings_file:
        for line in embeddings_file:
            entries = line.split()
            word = entries[0]
            if word not in embeddings_index:
                embedding_vector = np.asarray(entries[1:], dtype = np.float32)
                if embedding_vector.shape[0] > embedding_dim:
                    embedding_dim = embedding_vector.shape[0]
                embeddings_index[word] = embedding_vector

    word_index = tokenizer.word_index
    nb_words = min(max_features, len(word_index))
    embedding_matrix = np.zeros((nb_words, embedding_dim))
    missing = 0
    for word, i in word_index.items():
        if i >= max_features:
            continue
        embedding_vector = embeddings_index.get(word)
        if embedding_vector is not None:
            embedding_matrix[i] = embedding_vector
        else:
            missing += 1
            
    print("Number of words in corpus but not in embeddings file = %d" % (missing))
    return embedding_matrix

class parameters:
    max_features = 40000
    max_length = 200
    
if __name__ == '__main__':
    import argparse
    
    
    parser = argparse.ArgumentParser(description="Classify a set of website comments as offensive", 
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-i", "--input", help="input training comments, with toxic outputs", required = True)
    parser.add_argument("-a", "--augment", help="augmented training input lookup", required = False)    
    parser.add_argument("-e", "--embedding", help="use alternate embeddings file", required = False)    
    
    parser.add_argument("-t", "--test", help="input test comments, without toxic outputs", required = True)
    
    parser.add_argument("-w", "--weights", help="output model file", required = True)
    parser.add_argument("-l", "--log", help="output log file", required = True)
    
    parser.add_argument("-s", "--split", help="validation split", required = False, default = 0.05, type = float)
    parser.add_argument("-b", "--batchsize", help="batchsize", required = False, default = 32, type = int)
    parser.add_argument("-r", "--lr", help="learning rate", required = False, default = 0.001, type = float)
    parser.add_argument("-n", "--epochs", help="number of epochs", required = False, default = 2, type = int)
    
    args = parser.parse_args()

    train = pd.read_csv(args.input)
    test = pd.read_csv(args.test)
    
    Xids = train["id"]
    X = train["comment_text"].fillna("fillna").values
    Y = train[["toxic", "severe_toxic", "obscene", "threat", "insult", "identity_hate"]].values
    Xtest = test["comment_text"].fillna("fillna").values

    if args.augment is not None:
        augment = pd.read_csv(args.augment)
        XAugLookup = dict(augment[["id","comment_text"]].fillna("fillna").values)
        
        randomIndexes = np.random.permutation(len(X))
        for randomIndex in randomIndexes:
            if np.random.random() < 0.25 and Xids[randomIndex] in XAugLookup:
                X[randomIndex] = XAugLookup[Xids[randomIndex]]

    tokenizer = text.Tokenizer(num_words=parameters.max_features)
    
    tokenizer.fit_on_texts(list(X) + list(Xtest))

    Xs = tokenizer.texts_to_sequences(X)
    
    Xsp = sequence.pad_sequences(Xs, maxlen=parameters.max_length)

    embeddingMatrix = get_embeddings(args.embedding, parameters.max_features, tokenizer)
    
    model = get_model(parameters.max_features, parameters.max_length, embeddingMatrix)
        
    model.compile(loss='binary_crossentropy', optimizer=Adam(lr = args.lr), metrics=['accuracy'])
    
    print(model.summary())
    
    csvlogger = CSVLogger(args.log, append=True)
    checkPointer = ModelCheckpoint(filepath=args.weights, verbose = 1, save_best_only = True)
    hist = model.fit(Xsp, Y, batch_size=args.batchsize, epochs=args.epochs, validation_split = args.split,
                     callbacks=[csvlogger, checkPointer], verbose=1, shuffle=True)
        